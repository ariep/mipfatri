outputSeparator() {
  stty size | perl -ale 'print "─"x$F[1]'
}

outputDoubleSeparator() {
  stty size | perl -ale 'print "="x$F[1]'
}

debug() {
  if [ -n "${DEBUG-}" ]
  then
    echo "[DEBUG]" $@ >&2
  fi
}

warn() {
  echo "[WARNING]" $@ >&2
}

readConfig() {
  yq="yq r /config/mipfatri.yaml"

  # Read config.
  threshold=$($yq 'threshold')
  parts=$($yq 'parts[*].name')
  partCount=$(wc -l <<<"$parts")

  echo "Configuration:"
  echo "  threshold: $threshold"
  echo "  parts:"
  totalWeight=0
  declare -g -A disksByID
  for p in $(seq 1 $partCount)
  do
    partIndex=$((p - 1))
    name=$($yq "parts[$partIndex].name")
    weight=$($yq "parts[$partIndex].weight")
    totalWeight=$(($totalWeight + $weight))
    echo "  - $name (weight $weight)"
    diskCount=$($yq "parts[$partIndex].disks[*].id" | grep '^- ' | wc -l)
    for d in $(seq 1 $diskCount)
    do
      diskIndex=$((d - 1))
      diskID=$($yq "parts[$partIndex].disks[$diskIndex].id")
      diskName=$($yq "parts[$partIndex].disks[$diskIndex].name")
      echo "    + disk \"$diskName\""
      disksByID[$diskID]="$partIndex $diskIndex"
    done
  done
  echo "  total weight: $totalWeight"
}

initialise() {
  mkdir -p "/state/versions" "/state/logs/versions"
  stateFile="/state/main.yaml"
  if [ -f "$stateFile" ]
  then
    lastVersion=$(yq r "$stateFile" "lastVersion")
    if [ "$lastVersion" = "null" ]
    then
      unset lastVersion
    else
      echo "The current version of the vault is $lastVersion"
      versionLogFile="/state/logs/versions/$lastVersion.log"
    fi
  else
    # Initialise empty state file.
    echo '---' > "$stateFile"
  fi
  if [ -z "${lastVersion-}" ]
  then
    echo "The vault has not been created yet."
  fi
  logFile="/state/logs/main.log"

  debug "Creating temporary workspace..."
  mkdir /vault
  mount -t ramfs ramfs /vault

  mkdir /vault/{in,out}

  interruptCounter=0
}

log() {
  echo "[$(date "+%FT%T %z")]" $@ >> "$logFile"
}

versionLog() {
  echo "[$(date "+%FT%T %z")]" $@ >> "$versionLogFile"
}

waitForMoreDisks() {
  echo "Waiting for new disk to be inserted..." >&2
  trap '' SIGINT
  if ! device=$( \
    trap exit SIGINT && \
    inotifywait \
      -q \
      -e create \
      --format "%f" \
      --exclude "[^1]$|[^t].$|[^r]..$|[^a]...$|[^p]....$" \
      "/dev/disk/by-id"
  )
  then
    debug "inotify interrupted"
    if [ $interruptCounter -gt 0 ]
    then
      return 1
    fi
    interruptCounter+=1
  else
    debug "device: $device"
  fi
  trap exit SIGINT
  echo "$device"
}

createVersion() {
  echo "You are creating a new version of the vault contents."
  echo "Please enter a name for this version:"
  read -p "Version: " -e -i "$(date +%F)" version
  yq w -i "/state/main.yaml" "lastVersion" "$version"
  log "Created new version: $version"
  versionLogFile="/state/logs/versions/$version.log"
  versionLog "Created new version: $version"
  versionStateFile="/state/versions/$version.yaml"
  echo "---" > "$versionStateFile"
}

editContents() {
  mkdir -p "/vault/contents"
  versionLog "Started content editing."
  export shellContext="editContents"
  export version
  bash --init-file "/usr/local/share/mipfatri-init.sh"
  versionLog "Finished content editing."
}

mountDisk() {
  id=$1

  found=${disksByID[$id]-}
  if [ -z "$found" ]
  then
    debug "Disk with ID $id is not listed in config."
    return 1
  fi

  devicePath="/dev/disk/by-id/$id"
  if ! [ -e "$devicePath" ]
  then
    debug "Device $devicePath not present, not mounting."
    return 2
  fi

  device=$(realpath "$devicePath")
  if grep "$device" "/host-proc/1/mounts"
  then
    echo "Device $device is mounted outside the docker container." \
      "It is not safe to continue." \
      "Please unmount the device first."
    read -p "Press enter after unmounting."
  fi

  read partIndex diskIndex <<<"$found"
  diskName=$($yq "parts[$partIndex].disks[$diskIndex].name")

  mountPoint="/mnt/mipfatri/$diskName"
  mkdir -p "$mountPoint"
  debug "Mounting $device at $mountPoint."
  mount "$device" "$mountPoint"
  mipfatriDir="$mountPoint/.mipfatri"
  if [ ! -d "$mipfatriDir" ]
  then
    warn "The disk \"$diskName\" doesn't contain any mipfatri data yet."
    return 3
  fi
  local configFile="${mipfatriDir}/mipfatri.yaml"
  if [ -f "$configFile" ]
  then
    debug "Checking on-disk configuration."
    if ! diff "/config/mipfatri.yaml" "$configFile"
    then
      warn "Files /config/mipfatri.yaml and $configFile differ!"
      return 4
    fi
  else
    echo "The disk \"$diskName\" did not yet have the configuration stored on" \
      "it, copying now."
    cp "/config/mipfatri.yaml" "$configFile"
  fi
  if [ -n "${DEBUG-}" ]
  then
    tree -a "$mountPoint/.mipfatri"
  fi

  return 0
}

unmountDisk() {
  local devicePath="/dev/disk/by-id/$1"
  debug "Unmounting $devicePath"
  umount "$devicePath"
}

createParts() {
  echo "Vault contents:"
  tree -ah "/vault/contents"
  pushd "/vault/contents" >/dev/null
  tar czf "/vault/out.tgz" .
  popd >/dev/null
  label="mipfatri"
  local partLabels=""
  for p in $(seq 1 $partCount)
  do
    partIndex=$((p - 1))
    name=$($yq "parts[$partIndex].name")
    weight=$($yq "parts[$partIndex].weight")
    for j in $(seq 1 $weight)
    do
      partLabel="$name"
      if [ $weight -gt 1 ]
      then
        partLabel="$partLabel-$j-of-$weight"
      fi
      debug "part name: $partLabel"
      partLabels="$partLabels $partLabel"
    done
  done
  debug "part names: $partLabels"
  outputTemplate="/vault/out/#s.ss"
  DEBUG=${DEBUG-} ss-split $label $threshold $totalWeight $outputTemplate $partLabels </vault/out.tgz
}

writeParts() {
  local needMoreDisks=0
  declare -a disksNeeded
  for p in $(seq 1 $partCount)
  do
    partIndex=$((p - 1))
    diskCount=$($yq "parts[$partIndex].disks[*].id" | grep '^- ' | wc -l)
    partName=$($yq "parts[$partIndex].name")
    if [ $diskCount -eq 0 ]
    then
      warn "Part \"$partName\" has no disks configured."
    fi
    for d in $(seq 1 $diskCount)
    do
      diskIndex=$((d - 1))
      diskID=$($yq "parts[$partIndex].disks[$diskIndex].id")
      diskName=$($yq "parts[$partIndex].disks[$diskIndex].name")
      # Check if this disk has already been written.
      status=$(yq r "$versionStateFile" "disks[$diskName].status")
      debug "Status for disk \"$diskName\": $status"
      if [ "$status" = "written" ]
      then
        debug "This disk has already been written to; skipping."
        continue
      fi
      disksByID[$diskID]="$partIndex $diskIndex"
      mipfatriDir="/mnt/mipfatri/$diskName/.mipfatri"
      if ! mountDisk "$diskID"
      then
        debug "Disk \"$diskName\" is not available, cannot store to it."
        needMoreDisks=1
        disksNeeded+=($diskName)
        continue
      fi
      weight=$($yq "parts[$partIndex].weight")
      for j in $(seq 1 $weight)
      do
        partLabel="$partName"
        if [ $weight -gt 1 ]
        then
          partLabel="$partLabel-$j-of-$weight"
        fi
        sourceFile="/vault/out/$partLabel.ss"
        targetDir="$mipfatriDir/parts/$version"
        mkdir -p "$targetDir"
        targetFile="$targetDir/$partLabel.ss"
        debug "Copying $sourceFile to $targetFile"
        cp "$sourceFile" "$targetFile"
      done
      unmountDisk "$diskID"
      echo "  + Written part \"$partName\" to disk \"$diskName\"."
      versionLog "Written part \"$partName\" to disk \"$diskName\"."
      debug "Writing status to state file."
      yq w -i "$versionStateFile" "disks[$diskName].status" "written"
    done
  done

  if [ $needMoreDisks -eq 1 ]
  then
    echo "These disks still need to be written:"
    for d in ${disksNeeded[@]}
    do
      echo "- $d"
    done
    waitForMoreDisks
    writeParts
  else
    return 0
  fi
}

storeParts() {
  echo "Copying parts..."
  writeParts
}

diskRelevant() {
  debug "Checking relevance of disk \"$diskName\"..."
  if [ -n "$verifyAll" ]
  then
    if [ ${disksToVerify["$diskID"]+_} ]
    then
      debug "Disk \"$diskName\" was not yet verified, still relevant."
      return 0
    else
      debug "Disk \"$diskName\" was already verified, not relevant."
      return 1
    fi
  else
    if [ ${disksRead["$diskID"]+_} ]
    then
      debug "Disk \"$diskName\" was already read, not relevant."
      return 1
    else
      debug "Disk \"$diskName\" was not yet read, still relevant."
      return 0
    fi
  fi
}

readParts() {
  verifyAll=${1-}

  for p in $(seq 1 $partCount)
  do
    partIndex=$((p - 1))
    diskCount=$($yq "parts[$partIndex].disks[*].id" | grep '^- ' | wc -l)
    partName=$($yq "parts[$partIndex].name")
    for d in $(seq 1 $diskCount)
    do
      diskIndex=$((d - 1))
      diskID=$($yq "parts[$partIndex].disks[$diskIndex].id")
      diskName=$($yq "parts[$partIndex].disks[$diskIndex].name")
      if ! diskRelevant
      then
        debug "Disk \"$diskName\" is not deemed relevant, skipping."
        continue
      fi
      while true
      do
        if ! mountDisk "$diskID"
        then
          continue 2
        fi

        mipfatriDir="/mnt/mipfatri/$diskName/.mipfatri"

        weight=$($yq "parts[$partIndex].weight")

        for j in $(seq 1 $weight)
        do
          partLabel="$partName"
          if [ $weight -gt 1 ]
          then
            partLabel="$partLabel-$j-of-$weight"
          fi
          sourceFile="$mipfatriDir/parts/$lastVersion/$partLabel.ss"
          targetFile="/vault/in/$partLabel.ss"
          if [ -f "$targetFile" ]
          then
            # We already have this part from an earlier run.
            if [ -z "$verifyAll" ]
            then
              # Don't try to fetch it again.
              continue
            else
              # Fetch it again and compare.
              exitCode=0
              diff -q "$sourceFile" "$targetFile" || exitCode=$?
              if [ $exitCode -eq 0 ]
              then
                # Files are equal, all is well.
                echo "  + part \"$partName\" from disk \"$diskName\" equals counterpart."
                unset 'disksToVerify[$diskID]'
                continue
              else
                # Files do not match, panic.
                warn "Files $sourceFile and $targetFile differ!"
                echo "Dropping you to a shell to examine the situation."
                bash
                return 1
              fi
            fi
          fi
          if [ ! -f "$sourceFile" ]
          then
            warn "Cannot find $sourceFile on disk \"$diskName\" !"
            continue
          fi
          debug "Copying $sourceFile to $targetFile"
          cp $sourceFile $targetFile
          # Mark this disk as done.
          if [ -n "$verifyAll" ]
          then
            unset 'disksToVerify[$diskID]'
          else
            disksRead["$diskID"]="$diskName"
          fi

          if [ $j -eq 1 ]
          then
            echo "  + read part \"$partName\" from disk \"$diskName\""
          fi
        done

        unmountDisk "$diskID"
        break
      done
    done
  done
}

allDisksToVerify() {
  declare -g -A disksToVerify
  disksToVerify=()
  debug "Will need to verify:"
  for p in $(seq 1 $partCount)
  do
    partIndex=$((p - 1))
    diskCount=$($yq "parts[$partIndex].disks[*].id" | grep '^- ' | wc -l)
    for d in $(seq 1 $diskCount)
    do
      diskIndex=$((d - 1))
      diskName=$($yq "parts[$partIndex].disks[$diskIndex].name")
      diskID=$($yq "parts[$partIndex].disks[$diskIndex].id")
      disksToVerify["$diskID"]="$diskName"
      debug "+ \"$diskName\""
    done
  done
}

comparePartCount() {
  local count=0
  local status
  pushd "/vault/in" >/dev/null
  for p in *.ss
  do
    debug "Counting part $p"
    count=$(($count + 1))
  done
  if [ $count -lt $threshold ]
  then
    local countNeeded=$(($threshold - $count))
    echo "Only have $count parts so far." \
      "Need $countNeeded more to reach threshold of $threshold." \
      >&2
    echo "insufficient"
  else
    echo "sufficient"
  fi
  popd >/dev/null
}

recreateVault() {
  pushd "/vault/in" >/dev/null
  echo "Recreating vault contents..."
  ss-cat $threshold "/vault/in.tgz" *
  if [ -e "/vault/contents" ]
  then
    backupContents=$(mktemp -d "/vault/contents.old-XXX")
    rmdir "$backupContents"
    mv "/vault/contents" "$backupContents"
  fi
  mkdir "/vault/contents"
  tar xzf "/vault/in.tgz" -C "/vault/contents"
  echo "Done."
  popd >/dev/null
}

viewContents() {
  export shellContext="readContents"
  bash --init-file /usr/local/share/mipfatri-init.sh
}

collectParts() {
  verifyAll=${1-}
  if [ -n "$verifyAll" ]
  then
    debug "Verifying all disks."
    allDisksToVerify
  else
    declare -g -A disksRead
    disksRead=()
  fi
  while true
  do
    readParts "$verifyAll"
    if [ -z "$verifyAll" ]
    then
      status=$(comparePartCount)
      case "$status" in
        sufficient)
          break
          ;;
        insufficient)
          echo "Disks read so far:"
          for d in "${disksRead[@]}"
          do
            echo "  + $d"
          done
          waitForMoreDisks
          if [ $? -ne 0 ]
          then
            return 1
          fi
          ;;
        *)
          debug "Wrong status from comparePartCount: $status"
          return 2
      esac
    else
      # Check if all disks have been verified.
      numberToVerify=${#disksToVerify[@]}
      if [ $numberToVerify -gt 0 ]
      then
        echo "Still need to verify $numberToVerify disks:"
        for d in "${disksToVerify[@]}"
        do
          echo "  + $d"
        done
        waitForMoreDisks
      else
        # We verified all pieces.
        echo "Verification complete."
        versionStateFile="/state/versions/$lastVersion.yaml"
        yq w -i "$versionStateFile" 'verifications[+]' "$(date "+%FT%T %z")"
        versionLog "Verified all disks."
        break
      fi
    fi
  done
}

recoverFromDisk() {
  device=${1-}
  if [ -z "$device" ]
  then
    warn "No device passed to recover from."
    return 1
  fi
  if [ ! -e "$device" ]
  then
    warn "Device $device not found."
    return 2
  fi
  mkdir -p "/mnt/mipfatri-recovery"
  mount "$device" "/mnt/mipfatri-recovery"
  # Copy configuration from disk to container.
  cp "/mnt/mipfatri-recovery/.mipfatri/mipfatri.yaml" "/config/mipfatri.yaml"
  # Determine latest version of vault.
  newestVersion=$(ls -t "/mnt/mipfatri-recovery/.mipfatri/parts/" | head -n 1)
  echo "The latest version found on the recovery disk is $newestVersion." >&2
  umount "/mnt/mipfatri-recovery"
  mkdir -p "/state"
  echo "lastVersion: $newestVersion" >> "/state/main.yaml"
}

recoverVault() {
  interruptCounter=0
  echo "Please insert any of the disks to start the recovery process."
  device=$(waitForMoreDisks)
  echo "Recovering from device: $device"
  recoverFromDisk "/dev/disk/by-id/$device"
}

cleanup() {
  echo "Cleaning up."
  debug "Removing temporary workspace..."
  if grep -q "/vault" "/proc/mounts"
  then
    umount "/vault" || true
  fi
  debug "Unmounting any remaining disks..."
  if [ -d "/mnt/mipfatri" ]
  then
    cd "/mnt/mipfatri"
    for m in *
    do
      debug "Trying to unmount $m"
      if [ -n "${DEBUG-}" ]
      then
        umount "$m" || continue
      else
        umount "$m" 2>/dev/null || continue
      fi
    done
  fi
  exit
}
