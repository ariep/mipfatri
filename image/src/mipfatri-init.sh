hintSharedFolder() {
  echo 'There is a special shared folder that you can use to' \
    'exchange files between the vault and the host computer.' \
    'It is located at `/shared` in here, and at `./shared` from where' \
    'you started the `run.sh` script.'
}

hintExit() {
  echo 'Press Ctrl-D or `exit` when ready.'
}

echo 'Starting a shell from within the mipfatri program.'

if [ "${shellContext-}" = "editContents" ]
then
  if [ "${newVault}" = "1" ]
  then
    echo 'Place your vault contents in `/vault/contents`.'
  else
    echo 'The vault contents are located at `/vault/contents`.'
    echo 'You can edit them using any available program, such as vim or nano.'
  fi
  hintSharedFolder
  echo
  hintExit

  cd "/vault/contents"
  export PS1='(editing vault contents) $ '
fi

if [ "${shellContext-}" = "readContents" ]
then
  echo 'The vault contents are located at `/vault/contents`.'
  echo 'You can view them using any available program, such as vim or nano.'
  hintSharedFolder
  echo
  hintExit

  cd "/vault/contents"
  export PS1='(viewing vault contents) $ '
  tree
fi
