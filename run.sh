#!/bin/bash

image="ariep/mipfatri"
baseDir=$(pwd)
configDir="${baseDir}/config"
stateDir="${baseDir}/state"
sharedDir="${baseDir}/shared"

mkdir "$sharedDir"
sudo mount -t tmpfs tmpfs "$sharedDir"

docker run \
  --rm --privileged -it \
  -h mipfatri \
  -v /dev:/dev \
  -v /proc:/host-proc \
  -v "${configDir}:/config" \
  -v "${stateDir}:/state" \
  -v "${sharedDir}:/shared" \
  "$image" mipfatri $@

sudo umount "$sharedDir"
rmdir "$sharedDir"
