#!/bin/bash

set -o errexit

device=$1
label=$2

printf 'type=83' | sfdisk "$device"
echo "Sleeping for a second to allow devices to be scanned by kernel." >&2
sleep 1

mkfs.btrfs -f -L $label "${device}1"

tmpDir=$(mktemp -d "disk-$label.XXX")
mount "${device}1" "$tmpDir"

mipfatriDir="$tmpDir/.mipfatri"
mkdir "$mipfatriDir"

umount "$tmpDir"
rmdir "$tmpDir"

diskID=$(basename $(find -L /dev/disk/by-id -samefile "${device}1"))
echo "disk ID: $diskID" >&2
