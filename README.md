# mipfatri

mipfatri is a program to perform secret sharing: splitting up secret data into
parts, in such a way that you need to combine a configurable number – not
necessarily all – of the parts to recreate the original data, and possession of
fewer than the required number of parts doesn't reveal anything about the
original data, other than its size.

mipfatri uses [Shamir's secret sharing
scheme](https://en.wikipedia.org/wiki/Shamir%27s_Secret_Sharing). Under the
hood it uses the existing [`ssss`
implementation](http://point-at-infinity.org/ssss/). What it adds to that is:
* mipfatri lifts `ssss`'s restriction of a maximal secret size of 1024 bits (128
  bytes), by first splitting up the secret data into 128-byte chunks, and
  applying `ssss` to every one of those chunks separately;
* assuming you want to store the resulting shares on external disks, mipfatri
  takes care of writing new or updated files to all disks, and reading them back
  when recreating the secret;
* mipfatri allows storing the same share on multiple disks for redundancy, and
  can verify that they agree when recombining, providing a data integrity check.

## Prerequisites

To run the mipfatri program, you need:
* a Linux computer, including
  * [Docker](https://docs.docker.com/install/), as the program is run from a
    Docker container;
  * sudo privileges, to mount a tmpfs partition for exchanging data between the
    container and the host;
* Internet access, to download the mipfatri Docker image. If you prefer to run
  it from an air-gapped machine, you could [transfer the
  image](https://stackoverflow.com/questions/23935141/how-to-copy-docker-images-from-one-host-to-another-without-using-a-repository)
  beforehand;
* external disks (disks or simple usb keys), to store the shares. The exact
  number depends on your choice of sharing scheme and redundancy.

## Design your scheme

Decide on the number of shares that you want to create, and pick the threshold,
that is, how many shares are necessary to reconstruct the original data.

## Setting up

### Prepare disks

This is an optional step, as mipfatri will happily coexist with other data on
existing disks – it only writes to a top-level directory `.mipfatri`.

On the other hand, if you have a large set of blank disks that you want to take
into use for secret sharing, you can use the `format.sh` helper script to format
your external disks. 

Usage: `./format.sh $device $label` where `$device` is the full device
corresponding to the disk, such as `/dev/sdb`, and `$label` is a string label.
WARNING: this will repartition the disk, overwriting any existing data! Use with
care. This needs to be run as root.

The `format.sh` script will output the disk ID that you need in the next step.

### Configuration

Copy the file `config/mipfatri.yaml.example` to `config/mipfatri.yaml` and edit
it to reflect your sharing scheme parameters: threshold, list of disks,
including name, disk ID and weight.

## Recovery

If you do not have access to the machine with the state files, you may still
easily recover your data, as long as you have the required number of shares.

1. Make sure you satisfy the prerequisites (see above).
2. Download the mipfatri source, for example by cloning from gitlab:
    ```
    git clone https://gitlab.com/ariep/mipfatri.git
    ```
3. Enter the directory that was created by git (`cd mipfatri`), then run
    ```
    ./run.sh
    ```
    (you may need to enter your user password or otherwise authenticate to let
    the script run a `sudo` command).
4. The program will note that there is no configuration file in the expected
   location, and ask whether you want to start the recovery mode. Follow the
   instructions to recover your data.
